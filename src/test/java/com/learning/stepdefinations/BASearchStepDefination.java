package com.learning.stepdefinations;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class BASearchStepDefination {
	
public WebDriver driver;
	
	public static String expectedTile = "British Airways | Book Flights, Holidays, City Breaks & Check In Online";
	public static String expectedTextResult = "Here's what we've found for London.";
	
	@Given("^Open the chrome and launch the application$")
	public void launchAndNavigateToBrowser() {
		System.out.println("Open the Browser");
		
	}
	
	@When("^Enter the Username and Password$")
	public void enterLoginCredentials() {
		System.out.println("Enter the login Credentials");
	}
	
	@Then("^Reset the credential$")
	public void resetCredentials() {
		System.out.println("Reset the Credentials");
	}

	@Given("^User navigate to BA Home Page$")
	public void navigateToBAHomePage() {
		System.out.println("Inside Main Method");
		System.setProperty("webdriver.chrome.driver","C:\\Users\\44753\\Desktop\\Training\\Selenium_jars\\chromedriver_win32\\chromedriver.exe");
	    driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		System.out.println("After Browser Lunch");
		driver.get("https://ba.com");
		
	}
	
	@When("^User enter a search value in the Search option$")
	public void enterSearchValue() {
		WebElement searchButton = driver.findElement(By.xpath("//button[@id='header-search-button']//img[@alt='Search']"));
		searchButton.click();
		
		WebElement textInput = driver.findElement(By.id("search-input"));
		textInput.sendKeys("London");
		
		WebElement searchButton1 = driver.findElement(By.xpath("//button[@type='submit']//img[@alt='Search']"));
		searchButton1.click();
		
		WebElement resultsPage = driver.findElement(By.xpath("//span[@class='search-title-error']"));
			
	}
	
	@When("^User enter a search value \"([^\"]*)\" in the Search option$")
	public void enterSearchValue(String searchValue) {
		WebElement searchButton = driver.findElement(By.xpath("//button[@id='header-search-button']//img[@alt='Search']"));
		searchButton.click();
		
		WebElement textInput = driver.findElement(By.id("search-input"));
		textInput.sendKeys(searchValue);
		
		WebElement searchButton1 = driver.findElement(By.xpath("//button[@type='submit']//img[@alt='Search']"));
		searchButton1.click();
				
	}
	
	@Then("User should redirect search page and should see the search results$")
	public void searchResultsValidation()throws InterruptedException {
		WebElement resultsPage = driver.findElement(By.xpath("//span[@class='search-title-error']"));
		String actualText = resultsPage.getText();
		System.out.println("actualText"+actualText);
		/*if(expectedTextResult.equalsIgnoreCase(actualText)) {
			System.out.println("Test case 2 Passed");
		}else {
			System.out.println("Test case 2 Failed");
		}*/
		assertEquals(expectedTextResult, actualText);
		Thread.sleep(5000);
		
		System.out.println("After navigating to URL Lunch");
		
		driver.close();
			
	}
	
	@Then("User should redirect search page and should see the search results with \"([^\"]*)\" value$")
	public void searchResultsValidation(String searchValue)throws InterruptedException {
		WebElement resultsPage = driver.findElement(By.xpath("//span[@class='search-title-error']"));
		String actualText = resultsPage.getText();
		System.out.println("actualText"+actualText);
		
		if(searchValue.equalsIgnoreCase("London")) {
			expectedTextResult = "Here's what we've found for London.";
		}else if (searchValue.equalsIgnoreCase("Delhi")) {
			expectedTextResult = "Here's what we've found for Delhi.";
		}else if (searchValue.equalsIgnoreCase("USA")) {
			expectedTextResult = "Here's what we've found for USA.";
		}
		
		if(expectedTextResult.equalsIgnoreCase(actualText)) {
			System.out.println("Test case 2 Passed");
		}else {
			System.out.println("Test case 2 Failed");
		}
		
		Thread.sleep(5000);
		
		System.out.println("After navigating to URL Lunch");
		
		driver.close();
			
	}

}
