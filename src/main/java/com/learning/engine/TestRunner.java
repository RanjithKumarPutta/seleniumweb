package com.learning.engine;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/features",glue= "com.learning.stepdefinations",plugin = { "pretty", "html:target/cucumber-reports" })
public class TestRunner {

}
