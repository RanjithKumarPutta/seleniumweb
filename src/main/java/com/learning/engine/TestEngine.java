package com.learning.engine;

import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

@Log
public class TestEngine {

    protected static WebDriver driver;
    private static final String url = "https://ba.com";
    private static final String PATH = System.getProperty("user.dir") + "/src/main/resources/drivers/";

    public WebDriver loadApplicationURL(String browserName)throws InterruptedException {

        if (browserName.equalsIgnoreCase("firefox")) {

        }else if (browserName.equalsIgnoreCase("ie")) {

        }else if (browserName.equalsIgnoreCase("edge")) {

        }else {
            log.info("Inside chrome browser initilization");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--incognito");
            System.setProperty("webdriver.chrome.driver",PATH+"chromedriver.exe");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            log.info("Chrome Browser Initialized and launched");
            driver.get(url);
        }
        return driver;
    }

    public void doClickByID(String elementLocator){
        log.info("Before Click  ");
        WebElement element = driver.findElement(By.id(elementLocator));
        element.click();
        log.info("After Click  ");
    }

    public void doClickByXpath(String elementLocator){
        log.info("Before Click  ");
        WebElement element = driver.findElement(By.xpath(elementLocator));
        element.click();
        log.info("After Click  ");
    }

    public boolean doIsDisplayedByXpath(String elementLocator){
        boolean value;
        WebElement element = driver.findElement(By.xpath(elementLocator));
        value = element.isDisplayed();
        return value;
    }

    public void doSendKeysById(String elementLocator,String elementValue){
        WebElement element = driver.findElement(By.id(elementLocator));
        element.sendKeys(elementValue);
    }

    public void doSelectValueByVisibleText(String elementLocator,String elementValue){
        Select drpCountry = new Select(driver.findElement(By.id(elementLocator)));
        drpCountry.selectByVisibleText(elementValue);
    }

    public String doGetTextByXpath(String elementLocator){
        String actualValue;
        WebElement element  = driver.findElement(By.xpath(elementLocator));
        actualValue =element.getText();
        return actualValue;
    }
    /*
       Below Function is used close the browser session
     */
    public void closeBrowser() {
        driver.quit();
    }
}

